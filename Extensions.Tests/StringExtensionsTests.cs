using System;
using System.Text;
using FluentAssertions;
using Xunit;

namespace Extensions.Tests
{
    public class StringExtensionsTests
    {
        [Fact]
        public void RemoveDiacritics_PolishDiacriticsShouldBeRemoved()
        {
            // ARRANGE
            var givenString = "Zażółć gęślą jaźń. Używajmy polskich znaków";
            var extectedResult = "Zazolc gesla jazn. Uzywajmy polskich znakow";

            // ACT
            var result = givenString.RemoveDiacritics();

            // ASSERT
            result.Should().Be(extectedResult);
        }  

        [Fact]
        public void SanatizeEnpoint_SpecialCharactersShouldBeRemoved()
        {
            // ARRANGE
            var givenString = "this-?-sh/ould#$^be@remve\\d";
            var expectedResult = "this--shouldberemved";

            // ACT
            var result = givenString.SanatizeEnpoint();

            // ASSERT
            result.Should().Be(expectedResult);
        }

        [Fact]
        public void ToEndpoint_GenerateProperEndpointFromGivenString()
        {
            // ARRANGE
            var givenString = "Czyżby wkońcu się udało?";
            var expectedResult = "czyzby-wkoncu-sie-udalo";

            // ACT
            var result = givenString.ToEndpoint();

            // ASSERT
            result.Should().Be(expectedResult);
        } 
    }
}