using System;
using System.Text;
using System.Text.RegularExpressions;

namespace Extensions
{
    public static class StringExtensions
    {
        public static string RemoveDiacritics(this string source)
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            byte[] sourceBytes = Encoding.GetEncoding("ISO-8859-5").GetBytes(source);

            return Encoding.ASCII.GetString(sourceBytes);
        }

        public static string SanatizeEnpoint(this string source)
        {
            return Regex.Replace(source, "[^a-zA-Z0-9-]+", String.Empty, RegexOptions.Compiled);
        }

        public static string ToEndpoint(this string source)
        {
            return source.Replace(' ', '-')
                .ToLower()
                .RemoveDiacritics()
                .SanatizeEnpoint();
        }
    }
}